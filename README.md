# Movie-service

This simple application allow user to fetch movie, by a title, from https://omdbapi.com/ and saved to the database. To work correctly, `Movie-Service` needs additional logic provided by `Authentication-Service`, which could be find here: https://bitbucket.org/bartbaryn/authentication-service/src/development/.

## Run locally

Default endpoint: http://localhost:3000

1. Clone this repository
2. Run from root dir
3. Create .env file (like in .env.example)
4. Run `docker-compose build` to build images
   a. You could also run it locally using `npm run start` command
5. Start app using `docker-compose up -d`
6. To stop the movie service run `docker-compose down`

## Documentation

API documentation was created in Swagger and it is available here: `http://localhost:3000/api/docs/movie`

## TODO

- Create proper communication between micro-services using RabbitMQ or NATS
- Work more on docker files (I am not really familiar with docker-compose, so it should be slightly change)
- I used build-in Logger module, but I created my own package for making better logs here: https://bitbucket.org/bartbaryn/sample_code/src/master/
- Use orbs to add AWS and Slack logic to circleCI file. To automate process even more.
- Client Module should be created as separate module and connected with LoggerModule, to provide better logs system
- Filtering should be more extensive (e.g. pagination, order etc.)
