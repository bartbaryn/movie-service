export interface IMovie {
  id: string;
  title: string;
  released: Date;
  genre: string;
  director: string;
  creatorId: string;
  createdAt: Date;
}
