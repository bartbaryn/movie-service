import { Transform } from 'class-transformer';
import { IsEnum, IsOptional } from 'class-validator';

export enum OrderType {
  ascending = 'ASC',
  descending = 'DESC',
}

export abstract class SortQuery<T> {
  @IsOptional()
  abstract sortBy?: T;

  @IsEnum(OrderType)
  @Transform(({ value }) => (value ? value.toUpperCase() : undefined))
  order: OrderType = OrderType.ascending;
}
