import { InternalServerErrorException } from '@nestjs/common';
import axios from 'axios';
import { v4 } from 'uuid';

import { OMDB_API_RESPONSE_MOCK } from '../../test/mocks';
import { createTestingModule } from '../../test/testSetup';
import { OmdbService } from '../omdb.service';

jest.mock('axios');

describe('Service', () => {
  describe('Fetch movie by title', () => {
    let omdbService: OmdbService;

    beforeEach(async () => {
      const module = await createTestingModule().compile();

      omdbService = module.get<OmdbService>(OmdbService);

      await module.createNestApplication().init();
    });

    afterEach(() => {
      jest.clearAllMocks();
      jest.restoreAllMocks();
    });

    test('should return movie', async () => {
      axios.get = jest.fn().mockResolvedValueOnce({
        data: OMDB_API_RESPONSE_MOCK,
      });

      expect(await omdbService.fetchMovieByTitle(v4())).toBeDefined();
    });

    test(`should return error message, when omdbAPI has 'Error' in response`, async () => {
      const errorResponse = {
        Response: 'False',
        Error: 'Invalid API key!',
      };

      axios.get = jest.fn().mockResolvedValueOnce({
        data: errorResponse,
      });

      expect(await omdbService.fetchMovieByTitle(v4())).toMatchObject({
        data: errorResponse,
      });
    });

    test('should throw internal server error, when something goes wrong', async () => {
      omdbService.fetchMovieByTitle = jest.fn().mockRejectedValue(new InternalServerErrorException('Something goes wrong'));

      await expect(omdbService.fetchMovieByTitle(v4())).rejects.toThrow(InternalServerErrorException);
    });
  });
});
