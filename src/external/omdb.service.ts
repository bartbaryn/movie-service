import { Injectable, InternalServerErrorException, Logger } from '@nestjs/common';
import axios, { AxiosResponse } from 'axios';

import { IExternalMovie } from '../interfaces/externalMovie.interface';

export type OmdbApiErrorResponse = {
  Response: string;
  Error: string;
};

@Injectable()
export class OmdbService {
  private readonly logger = new Logger(OmdbService.name);

  async fetchMovieByTitle(title: string): Promise<AxiosResponse<IExternalMovie>> {
    try {
      return await axios.get<IExternalMovie>(`http://www.omdbapi.com/?apikey=${process.env.OMDB_API_KEY}&t=${title}`);
    } catch (error) {
      this.logger.error('Omdb service failed to fetch data', JSON.stringify(error));
      throw new InternalServerErrorException(error);
    }
  }
}
