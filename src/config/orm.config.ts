import { ConnectionOptions } from 'typeorm';

const mysqlConfig: ConnectionOptions = {
  type: 'mysql',
  host: process.env.DB_HOST || '127.0.0.1',
  port: parseInt(process.env.DB_PORT, 10) || 3307,
  username: process.env.DB_USER || 'root',
  password: process.env.DB_PASS || 'root',
  database: process.env.DB_NAME || 'development',
  entities: [`${__dirname}/../data/entity/*.entity{.ts,.js}`],
  migrations: [`${__dirname}/../data/entity/migration/**/*{.ts,.js}`],
  subscribers: [`${__dirname}/../data/subscriber/**/*.subscriber{.ts,.js}`],
  cli: {
    entitiesDir: 'src/data/entity',
    migrationsDir: 'src/data/migration',
    subscribersDir: 'src/data/subscriber',
  },
  logging: true,
  logger: 'advanced-console',
  synchronize: shouldSynchronize(),
  charset: 'utf8mb4',
  extra: {
    connectionLimit: databaseConnectionLimit(),
  },
};

const testConfig: ConnectionOptions = {
  ...mysqlConfig,
  database: 'test',
  dropSchema: true,
  synchronize: true,
  logging: false,
};

function shouldSynchronize(): boolean {
  switch (process.env.NODE_ENV) {
    case 'production':
    case 'staging':
      return false;
    default:
      return true;
  }
}

function databaseConnectionLimit(): number {
  return parseInt(process.env.DB_CONNECTION_LIMIT, 10) || 2;
}

export = process.env.NODE_ENV === 'test' ? testConfig : mysqlConfig;
