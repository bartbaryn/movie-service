import { ApiProperty } from '@nestjs/swagger';

import { IMovie } from '../../../interfaces/movie.interface';

export class MovieResponse implements IMovie {
  @ApiProperty({ type: 'string', format: 'uuid' })
  id: string;

  @ApiProperty({ type: 'string' })
  title: string;

  @ApiProperty({ type: 'string' })
  director: string;

  @ApiProperty({ type: 'string', format: 'date-time' })
  released: Date;

  @ApiProperty({ type: 'string' })
  genre: string;

  @ApiProperty({ type: 'string' })
  creatorId: string;

  @ApiProperty({ type: 'string', format: 'date-time' })
  createdAt: Date;
}
