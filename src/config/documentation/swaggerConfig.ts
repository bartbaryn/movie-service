import { DocumentBuilder } from '@nestjs/swagger';

import { version } from '../../../package.json';

const options = new DocumentBuilder().setTitle('MoviezzzApp').setDescription('API documentation of Moviezzz App').setVersion(version).build();

export { options };
