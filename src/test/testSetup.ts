import { Logger } from '@nestjs/common';
import { Test, TestingModuleBuilder } from '@nestjs/testing';
import { getConnection } from 'typeorm';

import { AppModule } from '../app.module';

export function createTestingModule(): TestingModuleBuilder {
  return Test.createTestingModule({
    imports: [AppModule],
  })
    .overrideProvider(Logger)
    .useValue({
      log: jest.fn(),
      warn: jest.fn(),
      error: jest.fn(),
      setContext: jest.fn(),
      debug: jest.fn(),
      verbose: jest.fn(),
    });
}

export function dropDatabase(): Promise<void> {
  return getConnection().synchronize(true);
}

export interface ValidationTest<T> {
  [key: string]: unknown;
  testedVariable: NonFunctionPropertyNames<T>;
  testDescription: string;
}

export interface BodyValidationTest<T> extends ValidationTest<T> {
  requestBody: { [P in NonFunctionPropertyNames<T>]?: unknown };
}

export type NonFunctionPropertyNames<T> = {
  [K in keyof T]: T[K] extends (...args: unknown[]) => unknown ? never : K;
}[keyof T] &
  string;
