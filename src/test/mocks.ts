import { v4 } from 'uuid';

import { IExternalMovie } from '../interfaces/externalMovie.interface';
import { IMovie } from '../interfaces/movie.interface';
import { IUser } from '../interfaces/user.interface';

export const AUTHORIZATION_HEADER = 'Authorization';
export const ACCESS_TOKEN_VALID = 'Bearer test';
export const ACCESS_TOKEN_INVALID = 'asd';

export const INVALID_STRING_VALUE = 123;

export const AUTHORIZED_USER_MOCK: IUser = {
  userId: '123',
  role: 'basic',
  name: 'Basic Thomas',
};

export const OMDB_API_RESPONSE_MOCK: IExternalMovie = {
  Title: 'Sad',
  Year: '1983',
  Rated: 'N/A',
  Released: '05 May 1957',
  Runtime: '31 min',
  Genre: 'Short, Drama',
  Director: 'Aleksandr Kaydanovskiy',
  Writer: 'Jorge Luis Borges (story)',
  Actors: 'N/A',
  Plot: 'N/A',
  Language: 'Russian',
  Country: 'Soviet Union',
  Awards: 'N/A',
  Poster: 'N/A',
  Ratings: [
    {
      Source: 'Internet Movie Database',
      Value: '7.4/10',
    },
  ],
  Metascore: 'N/A',
  imdbRating: '7.4',
  imdbVotes: '42',
  imdbID: 'tt0123857',
  Type: 'movie',
  DVD: 'N/A',
  BoxOffice: 'N/A',
  Production: 'N/A',
  Website: 'N/A',
  Response: 'True',
};

export const mockMovies = (numberOfMovies: number, options?: { title?: string }): IMovie[] => {
  const mockedMovies = [];

  for (let i = 0; i < numberOfMovies; i += 1) {
    const movieOptions = {
      title: `Title ${Math.floor(Math.random() * 10000)}`,
    };

    if (options?.title) {
      movieOptions.title = options.title;
    }

    mockedMovies.push({
      ...movieOptions,
      id: v4(),
      released: new Date(),
      genre: v4(),
      director: v4(),
      creatorId: '123',
    });
  }

  return mockedMovies;
};

export function generateSortQueries<T>(allowedKeys: string[]): T[] {
  const queries = [];
  const sortOrders = ['ASC', 'DESC'];

  allowedKeys.map(sortKey => {
    sortOrders.map(sortOrder => {
      queries.push({
        sortBy: sortKey,
        order: sortOrder,
      });
    });
  });

  return queries;
}
