import { Logger } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { Transport } from '@nestjs/microservices';
import { SwaggerModule } from '@nestjs/swagger';

import { AppModule } from './app.module';
import { options } from './config/documentation/swaggerConfig';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.setGlobalPrefix('api');

  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('api/docs/movie', app, document);

  app.connectMicroservice({
    transport: Transport.TCP,
    options: {
      host: process.env.MICROSERVICE_TRANPORT_HOST || 'localhost',
      port: process.env.MICROSERVICE_TRANPORT_PORT || 3100,
    },
  });

  await app.startAllMicroservicesAsync();
  await app.listen(process.env.MOVIE_SERVICE_PORT);
  Logger.log(`Service is listening on port: ${process.env.MOVIE_SERVICE_PORT}`, 'NestApplication');
}
bootstrap();
