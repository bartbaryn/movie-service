import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsEnum, IsOptional, Length } from 'class-validator';

import { SortQuery } from '../../utils/sorting/sortQuery';

export enum MovieSortBy {
  CreatedAt = 'createdAt',
  Title = 'title',
}

export class MovieFilterQuery extends SortQuery<MovieSortBy> {
  @ApiPropertyOptional({
    type: 'enum',
    enum: Object.values(MovieSortBy),
  })
  @IsOptional()
  @IsEnum(MovieSortBy, { message: `sortBy must be a valid enum value: ${Object.values(MovieSortBy)}` })
  sortBy?: MovieSortBy = MovieSortBy.CreatedAt;

  @ApiPropertyOptional({
    type: 'string',
  })
  @IsOptional()
  @Length(1, 255)
  title?: string;
}
