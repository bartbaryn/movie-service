import { Body, Controller, Get, Post, Query } from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiBearerAuth,
  ApiCreatedResponse,
  ApiInternalServerErrorResponse,
  ApiOkResponse,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';

import { MovieResponse } from '../config/documentation/responses/movie.response';
import { IMovie } from '../interfaces/movie.interface';
import { IUser } from '../interfaces/user.interface';
import { Auth } from './guards/decorators/auth.decorator';
import { AuthorizedUser } from './guards/decorators/authorizedUser.decorator';
import { CreateMovieBody } from './input/createMovie.body';
import { MovieFilterQuery } from './input/moviesFilter.query';
import { MovieService } from './movie.service';

@ApiTags('Movie')
@Controller('movies')
export class MovieController {
  constructor(private readonly movieService: MovieService) {}

  @Post()
  @Auth()
  @ApiBearerAuth()
  @ApiCreatedResponse({ type: MovieResponse })
  @ApiBadRequestResponse({ description: 'Validation error' })
  @ApiUnauthorizedResponse({ description: 'User in unauthorized' })
  @ApiInternalServerErrorResponse({ description: 'Movie module failed to create a movie' })
  async createMovie(@Body() createMovieBody: CreateMovieBody, @AuthorizedUser() user: IUser): Promise<IMovie> {
    return this.movieService.createMovie(createMovieBody, user);
  }

  @Get()
  @Auth()
  @ApiBearerAuth()
  @ApiOkResponse({ type: MovieResponse, isArray: true })
  @ApiBadRequestResponse({ description: 'Validation error' })
  @ApiUnauthorizedResponse({ description: 'User in unauthorized' })
  @ApiInternalServerErrorResponse({ description: 'Movie module failed to find a movie' })
  async findMovies(@Query() filter: MovieFilterQuery, @AuthorizedUser() user: IUser): Promise<IMovie[]> {
    return this.movieService.findMovie(filter, user.userId);
  }
}
