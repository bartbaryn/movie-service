import { Module } from '@nestjs/common';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { TypeOrmModule } from '@nestjs/typeorm';

import { MovieDao } from '../data/dao/movie.dao';
import { Movie } from '../data/entity/movie.entity';
import { OmdbModule } from '../external/omdb.module';
import { AUTHORIZATION_SERVICE } from '../utils/constants';
import { MovieController } from './movie.controller';
import { MovieService } from './movie.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([Movie]),
    OmdbModule,
    ClientsModule.register([
      {
        name: AUTHORIZATION_SERVICE,
        transport: Transport.TCP,
        options: {
          host: process.env.CLIENT_TRANPORT_HOST || 'localhost',
          port: parseInt(process.env.CLIENT_TRANPORT_PORT, 10) || 4100,
        },
      },
    ]),
  ],
  controllers: [MovieController],
  providers: [MovieService, MovieDao],
})
export class MovieModule {}
