import { SetMetadata } from '@nestjs/common';

export enum AuthorizationType {
  Header = 'header',
}

export interface AuthOptions {
  types?: AuthorizationType[];
}

export const AUTH_OPTIONS_METADATA_NAME = 'auth:options';

// tslint:disable-next-line: variable-name
export const AuthOptionsDecorator = (options?: AuthOptions) => SetMetadata(AUTH_OPTIONS_METADATA_NAME, options);
