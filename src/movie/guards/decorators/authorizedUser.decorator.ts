import { createParamDecorator, ExecutionContext } from '@nestjs/common';

// tslint:disable-next-line: variable-name
export const AuthorizedUser = createParamDecorator((data: string, ctx: ExecutionContext) => {
  const request = ctx.switchToHttp().getRequest();
  return data ? request.user?.[data] : request.user;
});
