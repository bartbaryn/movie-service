import { applyDecorators, UseFilters, UseGuards } from '@nestjs/common';

import { AuthGuard } from '../auth.guard';
import { UnauthorizedExceptionFilter } from '../filters/unauthorizedException.filter';
import { AuthOptions, AuthOptionsDecorator, AuthorizationType } from './authOptions.decorator';

export const defaultAuthOptions: AuthOptions = {
  types: [AuthorizationType.Header],
};

// eslint-disable-function-name tslint:disable: function-name
export function Auth(options: AuthOptions = {}) {
  const authOptions = Object.assign({}, defaultAuthOptions, options);

  if (authOptions.types.length === 0) {
    throw new Error('At lease one type of authorization must be provided');
  }

  return applyDecorators(UseFilters(UnauthorizedExceptionFilter), UseGuards(AuthGuard), AuthOptionsDecorator(authOptions));
}
