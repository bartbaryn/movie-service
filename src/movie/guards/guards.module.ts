import { Global, Module } from '@nestjs/common';
import { ClientsModule, Transport } from '@nestjs/microservices';

import { AUTHORIZATION_SERVICE } from '../../utils/constants';
import { AuthGuard } from './auth.guard';

@Global()
@Module({
  imports: [
    ClientsModule.register([
      {
        name: AUTHORIZATION_SERVICE,
        transport: Transport.TCP as number,
        options: {
          host: process.env.CLIENT_TRANPORT_HOST || 'localhost',
          port: process.env.CLIENT_TRANPORT_PORT || 4100,
        },
      },
    ]),
  ],
  providers: [AuthGuard],
  exports: [AuthGuard],
})
export class GuardsModule {}
