import { CanActivate, ExecutionContext, Inject, Injectable, UnauthorizedException } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { timeout } from 'rxjs/operators';

import { IUser } from '../../interfaces/user.interface';
import { AUTHORIZATION_SERVICE } from '../../utils/constants';

const AUTHORIZATION_HEADER_NAME = 'Authorization';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(
    @Inject(AUTHORIZATION_SERVICE)
    private readonly client: ClientProxy,
  ) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest();
    const headerValue = request.header(AUTHORIZATION_HEADER_NAME);

    if (!headerValue) {
      throw new UnauthorizedException('Missing header');
    }

    const user = await this.client
      .send({ cmd: 'verifyToken' }, { jwt: headerValue?.split(' ')[1] })
      .pipe(timeout(5000))
      .toPromise<IUser>();

    if (!user) {
      throw new UnauthorizedException('The user is unauthorized');
    }

    request.user = user;

    return true;
  }
}
