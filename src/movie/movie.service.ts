import { BadRequestException, Injectable } from '@nestjs/common';

import { MovieDao } from '../data/dao/movie.dao';
import { IMovie } from '../interfaces/movie.interface';
import { IUser } from '../interfaces/user.interface';
import { CreateMovieBody } from './input/createMovie.body';
import { MovieFilterQuery } from './input/moviesFilter.query';

@Injectable()
export class MovieService {
  constructor(protected readonly movieDao: MovieDao) {}

  async createMovie(data: CreateMovieBody, user: IUser): Promise<IMovie> {
    const { role, userId } = user;

    if (role === 'basic' && (await this.isLimitHasReached(userId))) {
      throw new BadRequestException('The monthly limit has been reached');
    }
    return this.movieDao.createMovie(data, userId);
  }

  async findMovie(data: MovieFilterQuery, creatorId: string): Promise<IMovie[]> {
    return this.movieDao.findMovies(data, creatorId);
  }

  private async isLimitHasReached(userId: string): Promise<boolean> {
    return (await this.movieDao.findNumberOfMoviesForCurrentMonth(userId)) >= 5;
  }
}
