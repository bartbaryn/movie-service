import { HttpStatus, INestApplication } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import axios from 'axios';
import { of } from 'rxjs';
import request from 'supertest';
import { v4 } from 'uuid';

import { MovieDao } from '../../data/dao/movie.dao';
import {
  ACCESS_TOKEN_INVALID,
  ACCESS_TOKEN_VALID,
  AUTHORIZATION_HEADER,
  AUTHORIZED_USER_MOCK,
  INVALID_STRING_VALUE,
  OMDB_API_RESPONSE_MOCK,
} from '../../test/mocks';
import { BodyValidationTest, createTestingModule, dropDatabase } from '../../test/testSetup';
import { AUTHORIZATION_SERVICE } from '../../utils/constants';
import { CreateMovieBody } from '../input/createMovie.body';

jest.mock('axios');

describe('Create movie', () => {
  let app: INestApplication;
  let client: ClientProxy;
  let movieDao: MovieDao;

  const BASE_ENDPOINT = '/movies';
  const title = 'Sad';
  const numberOfMovies = 5;
  const createMovieBody: CreateMovieBody = { title };

  beforeAll(async () => {
    const module = await createTestingModule().compile();

    app = module.createNestApplication();

    await app.init();

    client = app.get(AUTHORIZATION_SERVICE);
    movieDao = module.get<MovieDao>(MovieDao);
  });

  afterEach(() => {
    jest.clearAllMocks();
    jest.restoreAllMocks();
  });

  afterAll(async () => {
    await dropDatabase();
    await app.close();
  });

  describe('Movie module', () => {
    describe('Authorization', () => {
      beforeEach(async () => {
        axios.get = jest.fn().mockResolvedValueOnce({
          data: {
            ...OMDB_API_RESPONSE_MOCK,
            Title: title,
          },
        });
      });

      test('should return 401 Unauthorized, when no access token is passed', async () => {
        await request(app.getHttpServer()).post(BASE_ENDPOINT).set(AUTHORIZATION_HEADER, '').send(createMovieBody).expect(HttpStatus.UNAUTHORIZED);
      });

      test('should return 401 Unauthorized, when invalid token is passed', async () => {
        jest.spyOn(client, 'send').mockImplementationOnce(() => of(null));

        await request(app.getHttpServer())
          .post(`${BASE_ENDPOINT}`)
          .set(AUTHORIZATION_HEADER, ACCESS_TOKEN_INVALID)
          .send(createMovieBody)
          .expect(HttpStatus.UNAUTHORIZED);
      });

      test('should return 201 CREATED, when valid token is passed', async () => {
        jest.spyOn(client, 'send').mockImplementationOnce(() => of(AUTHORIZED_USER_MOCK));

        await request(app.getHttpServer())
          .post(BASE_ENDPOINT)
          .set(AUTHORIZATION_HEADER, ACCESS_TOKEN_VALID)
          .send(createMovieBody)
          .expect(HttpStatus.CREATED);
      });
    });

    describe('Controller', () => {
      beforeEach(() => {
        jest.spyOn(client, 'send').mockImplementationOnce(() => of(AUTHORIZED_USER_MOCK));
      });

      test('should return created movie', async () => {
        axios.get = jest.fn().mockResolvedValueOnce({
          data: {
            ...OMDB_API_RESPONSE_MOCK,
            Title: title,
          },
        });

        const response = await request(app.getHttpServer())
          .post(BASE_ENDPOINT)
          .set(AUTHORIZATION_HEADER, ACCESS_TOKEN_VALID)
          .send(createMovieBody)
          .expect(HttpStatus.CREATED);

        expect(response.body).toBeDefined();
        expect(response.body.title).toBe(title);
        expect(response.body.released).toBeDefined();
        expect(response.body.director).toBeDefined();
        expect(response.body.genre).toBeDefined();
        expect(response.body.createdAt).toStrictEqual(expect.any(String));
      });

      test('should return bad request exception, when the user exceed ', async () => {
        jest.spyOn(movieDao, 'findNumberOfMoviesForCurrentMonth').mockImplementationOnce(async () => numberOfMovies);

        await request(app.getHttpServer())
          .post(BASE_ENDPOINT)
          .set(AUTHORIZATION_HEADER, ACCESS_TOKEN_VALID)
          .send(createMovieBody)
          .expect(HttpStatus.BAD_REQUEST);
      });

      test('should return not found exception, when provided movie does not exist', async () => {
        const errorResponse = {
          Response: 'False',
          Error: 'Movie not found!',
        };

        axios.get = jest.fn().mockResolvedValueOnce({
          data: errorResponse,
        });

        const response = await request(app.getHttpServer()).post(BASE_ENDPOINT).set(AUTHORIZATION_HEADER, ACCESS_TOKEN_VALID).send({ title: v4() });

        expect(response.body.statusCode).toBe(HttpStatus.NOT_FOUND);
        expect(response.body.message).toBe(errorResponse.Error);
      });
    });

    describe('Validation', () => {
      beforeEach(() => {
        jest.spyOn(client, 'send').mockImplementationOnce(() => of(AUTHORIZED_USER_MOCK));
      });

      const validationTests: BodyValidationTest<CreateMovieBody>[] = [
        {
          requestBody: {
            ...createMovieBody,
            title: INVALID_STRING_VALUE,
          },
          testedVariable: 'title',
          testDescription: 'is not a string',
        },
        {
          requestBody: {
            ...createMovieBody,
            title: undefined,
          },
          testedVariable: 'title',
          testDescription: 'is not provided',
        },
      ];

      validationTests.map(bodyTest => {
        test(`should return 400 BAD REQUEST when ${bodyTest.testedVariable} ${bodyTest.testDescription}`, async () => {
          const response = await request(app.getHttpServer())
            .post(BASE_ENDPOINT)
            .set(AUTHORIZATION_HEADER, ACCESS_TOKEN_VALID)
            .send(bodyTest.requestBody)
            .expect(HttpStatus.BAD_REQUEST);

          expect(response.body.message[0]).toBeDefined();
        });
      });
    });
  });
});
