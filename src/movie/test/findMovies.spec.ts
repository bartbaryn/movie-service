import { HttpStatus, INestApplication } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { of } from 'rxjs';
import request from 'supertest';
import { getRepository, Repository } from 'typeorm';

import { Movie } from '../../data/entity/movie.entity';
import { IMovie } from '../../interfaces/movie.interface';
import {
  ACCESS_TOKEN_INVALID,
  ACCESS_TOKEN_VALID,
  AUTHORIZATION_HEADER,
  AUTHORIZED_USER_MOCK,
  generateSortQueries,
  mockMovies,
} from '../../test/mocks';
import { createTestingModule, dropDatabase } from '../../test/testSetup';
import { AUTHORIZATION_SERVICE } from '../../utils/constants';
import { MovieFilterQuery } from '../input/moviesFilter.query';

describe('Find movies', () => {
  let app: INestApplication;
  let client: ClientProxy;
  let movieRepo: Repository<Movie>;
  let sortQueries: MovieFilterQuery[];

  const BASE_ENDPOINT = '/movies';
  const movies: IMovie[] = mockMovies(5);
  const expectedResponseArrayLength = 1;

  beforeAll(async () => {
    const module = await createTestingModule().compile();

    app = module.createNestApplication();

    await app.init();

    client = app.get(AUTHORIZATION_SERVICE);

    movieRepo = getRepository(Movie);

    await Promise.all(await movies.map(async movie => await movieRepo.save(movieRepo.create(movie))));
  });

  afterEach(() => {
    jest.clearAllMocks();
    jest.restoreAllMocks();
  });

  afterAll(async () => {
    await dropDatabase();
    await app.close();
  });

  describe('Movie module', () => {
    describe('Authorization', () => {
      test('should return 401 Unauthorized, when no access token is passed', async () => {
        await request(app.getHttpServer()).get(BASE_ENDPOINT).set(AUTHORIZATION_HEADER, '').query({}).expect(HttpStatus.UNAUTHORIZED);
      });

      test('should return 401 Unauthorized, when invalid token is passed', async () => {
        jest.spyOn(client, 'send').mockImplementationOnce(() => of(null));

        await request(app.getHttpServer())
          .post(BASE_ENDPOINT)
          .set(AUTHORIZATION_HEADER, ACCESS_TOKEN_INVALID)
          .query({})
          .expect(HttpStatus.UNAUTHORIZED);
      });

      test('should return 200 OK, when valid token is passed', async () => {
        jest.spyOn(client, 'send').mockImplementationOnce(() => of(AUTHORIZED_USER_MOCK));

        await request(app.getHttpServer()).get(BASE_ENDPOINT).set(AUTHORIZATION_HEADER, ACCESS_TOKEN_VALID).query({}).expect(HttpStatus.OK);
      });
    });

    describe('Controller', () => {
      beforeEach(() => {
        jest.spyOn(client, 'send').mockImplementationOnce(() => of(AUTHORIZED_USER_MOCK));
      });

      test('should return all movies', async () => {
        const response = await request(app.getHttpServer())
          .get(BASE_ENDPOINT)
          .set(AUTHORIZATION_HEADER, ACCESS_TOKEN_VALID)
          .query({})
          .expect(HttpStatus.OK);

        expect(response.body).toBeDefined();
        expect(response.body).toHaveLength(movies.length);
      });

      test('should return requested movie', async () => {
        const response = await request(app.getHttpServer())
          .get(BASE_ENDPOINT)
          .set(AUTHORIZATION_HEADER, ACCESS_TOKEN_VALID)
          .query({
            title: movies[0].title,
          })
          .expect(HttpStatus.OK);

        expect(response.body).toBeDefined();
        expect(response.body).toHaveLength(expectedResponseArrayLength);
      });

      describe('sorting', () => {
        beforeEach(() => {
          jest.spyOn(client, 'send').mockImplementationOnce(() => of(AUTHORIZED_USER_MOCK));
        });

        sortQueries = generateSortQueries<MovieFilterQuery>(['createdAt', 'title']);

        sortQueries.forEach(async query => {
          test(`should return movies sorted by ${query.sortBy} in ${query.order} order`, async () => {
            const result = await request(app.getHttpServer())
              .get(BASE_ENDPOINT)
              .set(AUTHORIZATION_HEADER, ACCESS_TOKEN_VALID)
              .query(query)
              .expect(HttpStatus.OK);

            const content = result.body;

            if (query.order === 'ASC') {
              content.forEach((_, index) => {
                if (index < content.length - 1) {
                  expect(content[index][query.sortBy] <= content[index + 1][query.sortBy]).toBe(true);
                }
              });
            } else {
              content.forEach((_, index) => {
                if (index < content.length - 1) {
                  expect(content[index][query.sortBy] >= content[index + 1][query.sortBy]).toBe(true);
                }
              });
            }
          });
        });
      });
    });
  });
});
