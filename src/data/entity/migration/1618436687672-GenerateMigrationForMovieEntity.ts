import {MigrationInterface, QueryRunner} from "typeorm";

export class GenerateMigrationForMovieEntity1618436687672 implements MigrationInterface {
    name = 'GenerateMigrationForMovieEntity1618436687672'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("CREATE TABLE `movie` (`id` varchar(36) NOT NULL, `title` varchar(255) NOT NULL, `released` datetime NOT NULL, `genre` varchar(255) NOT NULL, `director` varchar(255) NOT NULL, `createdAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), PRIMARY KEY (`id`)) ENGINE=InnoDB");
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("DROP TABLE `movie`");
    }

}
