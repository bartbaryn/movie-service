import {MigrationInterface, QueryRunner} from "typeorm";

export class AddCreatorIDToMovieEntity1618849525739 implements MigrationInterface {
    name = 'AddCreatorIDToMovieEntity1618849525739'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `movie` ADD `creatorId` varchar(255) NOT NULL");
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `movie` DROP COLUMN `creatorId`");
    }

}
