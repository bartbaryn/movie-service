import { Column, CreateDateColumn, Entity, PrimaryGeneratedColumn } from 'typeorm';

import { IMovie } from '../../interfaces/movie.interface';

@Entity()
export class Movie implements IMovie {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  title: string;

  @Column({ type: 'datetime' })
  released: Date;

  @Column()
  genre: string;

  @Column()
  director: string;

  @Column()
  creatorId: string;

  @CreateDateColumn({ type: 'datetime' })
  createdAt: Date;
}
