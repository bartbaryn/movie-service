import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { OmdbService } from '../../external/omdb.service';
import { IMovie } from '../../interfaces/movie.interface';
import { CreateMovieBody } from '../../movie/input/createMovie.body';
import { MovieFilterQuery } from '../../movie/input/moviesFilter.query';
import { Movie } from '../entity/movie.entity';

export type OmdbApiErrorResponse = {
  Response: string;
  Error: string;
};

@Injectable()
export class MovieDao {
  constructor(
    @InjectRepository(Movie)
    private readonly movieRepository: Repository<Movie>,
    private readonly omdbService: OmdbService,
  ) {}

  async createMovie(data: CreateMovieBody, creatorId: string): Promise<IMovie> {
    const { title } = data;
    const fetchedMovieData = await this.omdbService.fetchMovieByTitle(title);

    if ('Error' in fetchedMovieData.data) {
      throw new NotFoundException((fetchedMovieData.data as OmdbApiErrorResponse)?.Error);
    }

    const movieToSave = this.movieRepository.create({
      creatorId,
      released: new Date(fetchedMovieData.data.Released),
      director: fetchedMovieData.data.Director,
      genre: fetchedMovieData.data.Genre,
      title: fetchedMovieData.data.Title,
    });

    return this.movieRepository.save(movieToSave);
  }

  findMovies(filter: MovieFilterQuery, creatorId: string): Promise<IMovie[]> {
    const { sortBy, order, title } = filter;

    const queryBuilder = this.movieRepository.createQueryBuilder('movie');
    queryBuilder.andWhere('movie.creatorId = :creatorId', { creatorId });

    if (title) {
      queryBuilder.andWhere('movie.title like :title', { title: `%${title}%` });
    }

    queryBuilder.orderBy(`movie.${sortBy}`, order);

    return queryBuilder.getMany();
  }

  async findNumberOfMoviesForCurrentMonth(creatorId: string): Promise<number> {
    return this.movieRepository
      .createQueryBuilder('movie')
      .andWhere('movie.creatorId = :creatorId', { creatorId })
      .andWhere(`movie.createdAt BETWEEN DATE_FORMAT(NOW() ,'%Y-%m-01') AND NOW()`)
      .getCount();
  }
}
